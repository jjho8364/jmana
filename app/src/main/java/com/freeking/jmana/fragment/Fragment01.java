package com.freeking.jmana.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.freeking.jmana.R;
import com.freeking.jmana.activity.ViewActivity;
import com.freeking.jmana.adapter.GridViewAdapterList;
import com.freeking.jmana.adapter.GridViewAdapterList01;
import com.freeking.jmana.item.GridViewItemList;
import com.freeking.jmana.item.GridViewItemList01;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class Fragment01 extends Fragment implements View.OnClickListener, LocationListener {
    private final String TAG = " Fragment01 - ";
    private ProgressDialog mProgressDialog;
    private GridView gridView;
    private GetGridView getGridView = null;
    private String baseUrl = "";
    private String firstUrl = "";
    private ArrayList<GridViewItemList01> listViewItemArr;

    private int adsCnt = 0;
    private InterstitialAd interstitialAd;
    AdRequest adRequest;
    SharedPreferences pref;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment01, container, false);

        baseUrl = getArguments().getString("baseUrl");
        firstUrl = getArguments().getString("firstUrl");

        pref= getActivity().getSharedPreferences("pref", MODE_PRIVATE); // 선언
        adsCnt =  Integer.parseInt(pref.getString("adsCnt",null));
        adRequest = new AdRequest.Builder().build();
        interstitialAd = new InterstitialAd(getActivity());
        interstitialAd.setAdUnitId("ca-app-pub-9440374750128282/9315453715");
        interstitialAd.loadAd(adRequest);

        gridView = (GridView)view.findViewById(R.id.gridview);

        getGridView = new GetGridView();
        getGridView.execute();

        return view;
    }

    public class GetGridView extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            listViewItemArr = new ArrayList<GridViewItemList01>();

            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document doc = null;
            try {

                doc = Jsoup.connect(baseUrl).timeout(20000).userAgent("Chrome").get();

                Elements lists = doc.select(".allList li a");

                for(int i=0 ; i<lists.size() ; i++){
                    String title = lists.get(i).select(".price").text();
                    String imgUrl = firstUrl + lists.get(i).select(".imgBox img").attr("src");
                    String viewUrl =  firstUrl + lists.get(i).attr("href");

                    GridViewItemList01 gridViewItemList01 = new GridViewItemList01(title, imgUrl, viewUrl);
                    listViewItemArr.add(gridViewItemList01);
                }
            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(getActivity() != null){
                gridView.setAdapter(new GridViewAdapterList01(getActivity(), listViewItemArr, R.layout.gridviewitem_list_01));

                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        if(adsCnt == 1){
                            adsCnt++;
                            SharedPreferences.Editor editor = pref.edit();// editor에 put 하기
                            editor.putString("adsCnt", "2"); //First라는 key값으로 id 데이터를 저장한다.
                            editor.commit(); //완료한다.
                            interstitialAd.show();
                        } else {
                            Intent intent = new Intent(getActivity(), ViewActivity.class);
                            intent.putExtra("listUrl", listViewItemArr.get(position).getListUrl());
                            intent.putExtra("firstUrl", firstUrl);
                            intent.putExtra("adsCnt", "0");
                            startActivity(intent);
                        }
                    }
                });
            }

            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(getGridView != null){
            getGridView.cancel(true);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(getGridView != null){
            getGridView.cancel(true);
        }
    }
}
