package com.freeking.jmana.item;

public class GridViewItemList01 {
    private String title;
    private String imgUrl;
    private String listUrl;

    public GridViewItemList01(String title, String imgUrl, String listUrl) {
        this.title = title;
        this.imgUrl = imgUrl;
        this.listUrl = listUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getListUrl() {
        return listUrl;
    }

    public void setListUrl(String listUrl) {
        this.listUrl = listUrl;
    }
}
