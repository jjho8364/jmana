package com.freeking.jmana.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.freeking.jmana.R;
import com.freeking.jmana.adapter.GridViewAdapterList;
import com.freeking.jmana.adapter.ViewActivityAdapter;
import com.freeking.jmana.fragment.Fragment02;
import com.freeking.jmana.item.ViewActivityItem;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;

public class ViewActivity extends FragmentActivity implements View.OnClickListener {
    private final String TAG = " ViewActivity - ";
    private ProgressDialog mProgressDialog;
    //private ListView listView;
    private WebView webView;
    private GetImageUrl getImageUrl = null;
    private String baseUrl = "";
    private String firstUrl = "";
    private ArrayList<ViewActivityItem> itemArr;

    private TextView preEps;
    private TextView nextEps;
    private String preUrl = "";
    private String nextUrl = "";

    private int adsCnt = 0;
    private InterstitialAd interstitialAd;
    AdRequest adRequest;
    SharedPreferences pref;

    private String html = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view);

        AdView adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        Intent mainIntent = getIntent();
        baseUrl = mainIntent.getStringExtra("listUrl");
        firstUrl = mainIntent.getStringExtra("firstUrl");

        //listView = (ListView)findViewById(R.id.listview);
        webView = (WebView) findViewById(R.id.webView);

        preEps = (TextView)findViewById(R.id.tv_preeps);
        nextEps = (TextView)findViewById(R.id.tv_nexteps);

        preEps.setOnClickListener(this);
        nextEps.setOnClickListener(this);

        getImageUrl = new GetImageUrl();
        getImageUrl.execute();

    }

    public class GetImageUrl extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            itemArr = new ArrayList<ViewActivityItem>();
            html = "";

            mProgressDialog = new ProgressDialog(ViewActivity.this);
            mProgressDialog.setTitle("이미지를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }
        @Override
        protected Void doInBackground(Void... params) {
            Document doc = null;

            try {
                doc = Jsoup.connect(baseUrl).timeout(15000).userAgent("chrome").get();
                Elements lists = doc.select(".listType.view li img");

                html += "<html>";
                html += "   <head><style>img{display: inline;height: auto;max-width: 100%;}</style></head>";
                html += "   <body>";
                html += "       <div>";

                for(int i=0 ; i<lists.size() ; i++) {
                    String imgUrl = lists.get(i).attr("src");
                    itemArr.add(new ViewActivityItem(imgUrl));

                    html += "           <img src='" + imgUrl + "'>";
                }

                html += "       <div>";
                html += "   </body>";
                html += "</html>";

                // preeps and next eps
                Elements btns = doc.select(".btnC a");

                for(int i=0 ; i<btns.size() ; i++) {
                    String epsUrl = firstUrl + btns.get(i).attr("href");
                    String text = btns.get(i).text();

                    if(text.equals("이전 보기")) preUrl = epsUrl;
                    if(text.equals("다음 보기")) nextUrl = epsUrl;
                }

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(ViewActivity.this != null){
                //listView.setAdapter(new ViewActivityAdapter(ViewActivity.this, itemArr, R.layout.viewactivityitem));
                webView.loadDataWithBaseURL(null, html, "text/html", "UTF-8", null);
            }

            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tv_preeps :
                if(preUrl !=null && !preUrl.equals("")) {
                    Log.d(TAG, "preUrl : " + preUrl);
                    Log.d(TAG, "firstUrl : " + firstUrl);
                    Intent intent = new Intent(ViewActivity.this, ViewActivity.class);
                    intent.putExtra("listUrl", preUrl);
                    intent.putExtra("firstUrl", firstUrl);
                    intent.putExtra("adsCnt", "0");
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "이전화가 없습니다.", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.tv_nexteps :
                if(nextUrl !=null && !nextUrl.equals("")) {
                    Log.d(TAG, "preUrl : " + preUrl);
                    Log.d(TAG, "firstUrl : " + firstUrl);
                    Intent intent = new Intent(ViewActivity.this, ViewActivity.class);
                    intent.putExtra("listUrl", preUrl);
                    intent.putExtra("firstUrl", firstUrl);
                    intent.putExtra("adsCnt", "0");
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "다음화가 없습니다.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }
}
