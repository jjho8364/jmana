package com.freeking.jmana.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.freeking.jmana.R;
import com.freeking.jmana.fragment.Fragment01;
import com.freeking.jmana.fragment.Fragment02;
import com.freeking.jmana.fragment.FragmentSearch;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;

public class MainActivity extends FragmentActivity implements View.OnClickListener {
    private final String TAG = " MainActivityTAG - ";

    private int mCurrentFragmentIndex;
    public final static int FRAGMENT_SEARCH = 99;
    public final static int FRAGMENT_ONE = 0;
    public final static int FRAGMENT_TWO = 1;
    public final static int FRAGMENT_THREE = 2;
    public final static int FRAGMENT_FOUR = 3;
    public final static int FRAGMENT_FIVE = 4;
    public final static int FRAGMENT_SIX = 5;
    public final static int FRAGMENT_SEVEN = 6;
    public final static int FRAGMENT_EIGHT = 7;
    public final static int FRAGMENT_NINE = 8;
    public final static int FRAGMENT_TEN = 9;
    public final static int FRAGMENT_ELEVEN = 10;
    public final static int FRAGMENT_TWELVE = 11;
    public final static int FRAGMENT_THIRTEEN = 12;
    public final static int FRAGMENT_FOURTEEN = 13;
    public final static int FRAGMENT_FIFTEEN = 14;
    public final static int FRAGMENT_SIXTEEN = 15;
    public final static int FRAGMENT_SEVENTEEN = 16;
    public final static int FRAGMENT_EIGHTTEEN = 17;
    public final static int FRAGMENT_NINETEEN = 18;
    public final static int FRAGMENT_TWENTY = 19;
    public final static int FRAGMENT_TWENTYONE = 20;
    public final static int FRAGMENT_TWENTYTWO = 21;
    public final static int FRAGMENT_TWENTYTHREE = 22;
    public final static int FRAGMENT_TWENTYTFOUR = 23;
    public final static int FRAGMENT_TWENTYFIVE = 24;
    public final static int FRAGMENT_TWENTYSIX = 25;
    public final static int FRAGMENT_TWENTYSEVEN = 26;
    public final static int FRAGMENT_TWENTYEIGHT = 27;
    public final static int FRAGMENT_TWENTYNINE = 28;
    public final static int FRAGMENT_THIRTY = 29;
    public final static int FRAGMENT_THIRTYONE = 30;
    public final static int FRAGMENT_THIRTYTWO = 31;
    public final static int FRAGMENT_THIRTYTHREE = 32;
    public final static int FRAGMENT_THIRTYFOUR = 33;

    private TextView fragment99;
    private TextView fragment01;
    private TextView fragment02;
    private TextView fragment03;
    private TextView fragment04;
    private TextView fragment05;
    private TextView fragment06;
    private TextView fragment07;
    private TextView fragment08;
    private TextView fragment09;
    private TextView fragment10;
    private TextView fragment11;
    private TextView fragment12;
    private TextView fragment13;
    private TextView fragment14;
    private TextView fragment15;
    private TextView fragment16;
    private TextView fragment17;
    private TextView fragment18;
    private TextView fragment19;
    private TextView fragment20;
    private TextView fragment21;
    private TextView fragment22;
    private TextView fragment23;
    private TextView fragment24;
    private TextView fragment25;
    private TextView fragment26;
    private TextView fragment27;
    private TextView fragment28;
    private TextView fragment29;
    private TextView fragment30;
    private TextView fragment31;
    private TextView fragment32;
    private TextView fragment33;
    private TextView fragment34;

    private String fragment99Url = "";
    private String fragment01Url = "";
    private String fragment02Url = "";
    private String fragment03Url = "";
    private String fragment04Url = "";
    private String fragment05Url = "";
    private String fragment06Url = "";
    private String fragment11Url = "";
    private String fragment12Url = "";
    private String fragment13Url = "";
    private String fragment14Url = "";
    private String fragment15Url = "";
    private String fragment16Url = "";
    private String fragment17Url = "";
    private String fragment18Url = "";
    private String fragment19Url = "";
    private String fragment20Url = "";
    private String fragment21Url = "";
    private String fragment22Url = "";
    private String fragment23Url = "";
    private String fragment25Url = "";
    private String fragment26Url = "";
    private String fragment27Url = "";
    private String fragment28Url = "";
    private String fragment29Url = "";
    private String fragment30Url = "";
    private String fragment31Url = "";
    private String fragment32Url = "";
    private String fragment33Url = "";
    private String fragment34Url = "";

    private String nextAppUrl = "";
    //private String mxPlayerUrl = "";
    private Date curDate;
    private String appStatus = "99";
    private String firstUrl = "";

    private int adsCnt = 0;
    InterstitialAd interstitialAd;
    InterstitialAd interstitialAd3;

    ArrayList<TextView> hideFragment;

    Date d1;
    Date d2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Intent mainIntent = getIntent();

        appStatus = mainIntent.getStringExtra("appStatus");
        firstUrl = mainIntent.getStringExtra("firstUrl");
        Log.d(TAG, "appStatus : " + appStatus);
        d1 = new Date();

        hideFragment= new ArrayList<TextView>();

        SharedPreferences pref= getSharedPreferences("pref", MODE_PRIVATE); // 선언
        String first = pref.getString("first",null);
        if(first != null && !appStatus.equals("3") && !appStatus.equals("4"))  appStatus = "1";

        if(appStatus.equals("1")){
            setContentView(R.layout.activity_main);

            curDate = new Date();

            AdView adView = (AdView) findViewById(R.id.adView);
            AdRequest adRequest = new AdRequest.Builder().build();
            adView.loadAd(adRequest);

            AdRequest adRequest2 = new AdRequest.Builder().build();
            interstitialAd = new InterstitialAd(this);
            interstitialAd.setAdUnitId("ca-app-pub-9440374750128282/7974451883");
            interstitialAd.loadAd(adRequest2);

            fragment99 = (TextView)findViewById(R.id.tv_fragment_search);
            fragment01 = (TextView)findViewById(R.id.tv_fragment01);
            fragment02 = (TextView)findViewById(R.id.tv_fragment02);
            fragment03 = (TextView)findViewById(R.id.tv_fragment03);
            fragment04 = (TextView)findViewById(R.id.tv_fragment04);
            fragment05 = (TextView)findViewById(R.id.tv_fragment05);
            fragment06 = (TextView)findViewById(R.id.tv_fragment06);
            fragment07 = (TextView)findViewById(R.id.tv_fragment07);
            fragment08 = (TextView)findViewById(R.id.tv_fragment08);
            fragment09 = (TextView)findViewById(R.id.tv_fragment09);
            fragment10 = (TextView)findViewById(R.id.tv_fragment10);
            fragment11 = (TextView)findViewById(R.id.tv_fragment11);
            fragment12 = (TextView)findViewById(R.id.tv_fragment12);
            fragment13 = (TextView)findViewById(R.id.tv_fragment13);
            fragment14 = (TextView)findViewById(R.id.tv_fragment14);
            fragment15 = (TextView)findViewById(R.id.tv_fragment15);
            fragment16 = (TextView)findViewById(R.id.tv_fragment16);
            fragment17 = (TextView)findViewById(R.id.tv_fragment17);
            fragment18 = (TextView)findViewById(R.id.tv_fragment18);
            fragment19 = (TextView)findViewById(R.id.tv_fragment19);
            fragment20 = (TextView)findViewById(R.id.tv_fragment20);
            fragment21 = (TextView)findViewById(R.id.tv_fragment21);
            fragment22 = (TextView)findViewById(R.id.tv_fragment22);
            fragment23 = (TextView)findViewById(R.id.tv_fragment23);
            fragment24 = (TextView)findViewById(R.id.tv_fragment24);
            fragment25 = (TextView)findViewById(R.id.tv_fragment25);
            fragment26 = (TextView)findViewById(R.id.tv_fragment26);
            fragment27 = (TextView)findViewById(R.id.tv_fragment27);
            fragment28 = (TextView)findViewById(R.id.tv_fragment28);
            fragment29 = (TextView)findViewById(R.id.tv_fragment29);
            fragment30 = (TextView)findViewById(R.id.tv_fragment30);
            fragment31 = (TextView)findViewById(R.id.tv_fragment31);
            fragment32 = (TextView)findViewById(R.id.tv_fragment32);
            fragment33 = (TextView)findViewById(R.id.tv_fragment33);
            fragment34 = (TextView)findViewById(R.id.tv_fragment34);

            fragment99.setOnClickListener(this);
            fragment01.setOnClickListener(this);
            fragment02.setOnClickListener(this);
            fragment03.setOnClickListener(this);
            fragment04.setOnClickListener(this);
            fragment05.setOnClickListener(this);
            fragment06.setOnClickListener(this);
            fragment07.setOnClickListener(this);
            fragment08.setOnClickListener(this);
            fragment09.setOnClickListener(this);
            fragment10.setOnClickListener(this);
            fragment11.setOnClickListener(this);
            fragment12.setOnClickListener(this);
            fragment13.setOnClickListener(this);
            fragment14.setOnClickListener(this);
            fragment15.setOnClickListener(this);
            fragment16.setOnClickListener(this);
            fragment17.setOnClickListener(this);
            fragment18.setOnClickListener(this);
            fragment19.setOnClickListener(this);
            fragment20.setOnClickListener(this);
            fragment21.setOnClickListener(this);
            fragment22.setOnClickListener(this);
            fragment23.setOnClickListener(this);
            fragment24.setOnClickListener(this);
            fragment25.setOnClickListener(this);
            fragment26.setOnClickListener(this);
            fragment27.setOnClickListener(this);
            fragment28.setOnClickListener(this);
            fragment29.setOnClickListener(this);
            fragment30.setOnClickListener(this);
            fragment31.setOnClickListener(this);
            fragment32.setOnClickListener(this);
            fragment33.setOnClickListener(this);
            fragment34.setOnClickListener(this);

            fragment99Url = mainIntent.getStringExtra("fragment99Url");
            fragment01Url = mainIntent.getStringExtra("fragment01Url");
            fragment02Url = mainIntent.getStringExtra("fragment02Url");
            fragment03Url = mainIntent.getStringExtra("fragment03Url");
            fragment04Url = mainIntent.getStringExtra("fragment04Url");
            fragment05Url = mainIntent.getStringExtra("fragment05Url");
            fragment06Url = mainIntent.getStringExtra("fragment06Url");
            fragment11Url = mainIntent.getStringExtra("fragment11Url");
            fragment12Url = mainIntent.getStringExtra("fragment12Url");
            fragment13Url = mainIntent.getStringExtra("fragment13Url");
            fragment14Url = mainIntent.getStringExtra("fragment14Url");
            fragment15Url = mainIntent.getStringExtra("fragment15Url");
            fragment16Url = mainIntent.getStringExtra("fragment16Url");
            fragment17Url = mainIntent.getStringExtra("fragment17Url");
            fragment18Url = mainIntent.getStringExtra("fragment18Url");
            fragment19Url = mainIntent.getStringExtra("fragment19Url");
            fragment20Url = mainIntent.getStringExtra("fragment20Url");
            fragment21Url = mainIntent.getStringExtra("fragment21Url");
            fragment22Url = mainIntent.getStringExtra("fragment22Url");
            fragment23Url = mainIntent.getStringExtra("fragment23Url");
            fragment25Url = mainIntent.getStringExtra("fragment25Url");
            fragment26Url = mainIntent.getStringExtra("fragment26Url");
            fragment27Url = mainIntent.getStringExtra("fragment27Url");
            fragment28Url = mainIntent.getStringExtra("fragment28Url");
            fragment29Url = mainIntent.getStringExtra("fragment29Url");
            fragment30Url = mainIntent.getStringExtra("fragment30Url");
            fragment31Url = mainIntent.getStringExtra("fragment31Url");
            fragment32Url = mainIntent.getStringExtra("fragment32Url");
            fragment33Url = mainIntent.getStringExtra("fragment33Url");
            fragment34Url = mainIntent.getStringExtra("fragment34Url");

            mCurrentFragmentIndex = FRAGMENT_ONE;     // 첫 Fragment 를 초기화
            fragmentReplace(mCurrentFragmentIndex);

            SharedPreferences.Editor editor = pref.edit();// editor에 put 하기
            editor.putString("first", "1"); //First라는 key값으로 id 데이터를 저장한다.
            editor.putString("adsCnt", "1"); //First라는 key값으로 id 데이터를 저장한다.
            editor.commit(); //완료한다.

        } else if(appStatus.equals("2")){
            setContentView(R.layout.maintenance);
            String maintenanceImgUrl = mainIntent.getStringExtra("maintenance");
            ImageView imgView = (ImageView)findViewById(R.id.img_maintenance);
            if(maintenanceImgUrl == null || maintenanceImgUrl.equals("")){
                imgView.setImageResource(R.drawable.no_image_png);
            } else {
                Picasso.with(this).load(maintenanceImgUrl).into(imgView);
            }
        } else if(appStatus.equals("3")){
            setContentView(R.layout.closed);
            String closedImgUrl = mainIntent.getStringExtra("closed");
            ImageView imgView = (ImageView)findViewById(R.id.img_closed);
            nextAppUrl = mainIntent.getStringExtra("nextAppUrl");
            if(closedImgUrl == null || closedImgUrl.equals("")){
                imgView.setImageResource(R.drawable.no_image_png);
            } else {
                Picasso.with(this).load(closedImgUrl).into(imgView);
            }
            Button btnClosed = (Button) findViewById(R.id.btn_closed);
            btnClosed.setOnClickListener(this);
        } else if(appStatus.equals("4")) {   // mx player
            setContentView(R.layout.mxplayer);
            //mxPlayerUrl = mainIntent.getStringExtra("mxPlayerUrl");
            //Button btnMxPlayer = (Button) findViewById(R.id.btn_mxplayer);
            //btnMxPlayer.setOnClickListener(this);
        } else if(appStatus.equals("9")) {   // mx player
            setContentView(R.layout.closed);
            String closedImgUrl = mainIntent.getStringExtra("closedImgUrl");
            nextAppUrl = mainIntent.getStringExtra("nextAppUrl");
            Log.d(TAG, "  " + nextAppUrl);
            ImageView imgView = (ImageView)findViewById(R.id.img_closed);
            Picasso.with(this).load(closedImgUrl).into(imgView);
            Button btnClosed = (Button) findViewById(R.id.btn_closed);
            btnClosed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent marketLaunch1 = new Intent(Intent.ACTION_VIEW);
                    marketLaunch1.setData(Uri.parse(nextAppUrl));
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(nextAppUrl));
                    startActivity(intent);
                }
            });
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.tv_fragment_search:
                offColorTv();
                fragment99.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_SEARCH;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment01:
                offColorTv();
                fragment01.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_ONE;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment02:
                offColorTv();
                fragment02.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_TWO;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment03:
                offColorTv();
                fragment03.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_THREE;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment04:
                offColorTv();
                fragment04.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_FOUR;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment05:
                offColorTv();
                fragment05.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_FIVE;
                fragmentReplace(mCurrentFragmentIndex);
                break;

            case R.id.tv_fragment06:
                offColorTv();
                fragment06.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_SIX;
                fragmentReplace(mCurrentFragmentIndex);
                break;

            case R.id.tv_fragment07:
                offColorTv();
                fragment07.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_SEVEN;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment24:
                offColorTv();
                fragment24.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_TWENTYTFOUR;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment08:
                offColorTv();
                fragment08.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_EIGHT;
                fragmentReplace(mCurrentFragmentIndex);
                break;

            case R.id.tv_fragment09:
                offColorTv();
                fragment09.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_NINE;
                fragmentReplace(mCurrentFragmentIndex);
                break;

            case R.id.tv_fragment10:
                offColorTv();
                fragment10.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_TEN;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment11:
                offColorTv();
                fragment11.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_ELEVEN;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment12:
                offColorTv();
                fragment12.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_TWELVE;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment13:
                offColorTv();
                fragment13.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_THIRTEEN;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment14:
                offColorTv();
                fragment14.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_FOURTEEN;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment15:
                offColorTv();
                fragment15.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_FIFTEEN;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment16:
                offColorTv();
                fragment16.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_SIXTEEN;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment17:
                offColorTv();
                fragment17.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_SEVENTEEN;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment18:
                offColorTv();
                fragment18.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_EIGHTTEEN;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment19:
                offColorTv();
                fragment19.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_NINETEEN;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment20:
                offColorTv();
                fragment20.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_TWENTY;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment21:
                offColorTv();
                fragment21.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_TWENTYONE;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment22:
                offColorTv();
                fragment22.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_TWENTYTWO;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment23:
                offColorTv();
                fragment23.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_TWENTYTHREE;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment25:
                offColorTv();
                fragment25.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_TWENTYFIVE;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment26:
                offColorTv();
                fragment26.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_TWENTYSIX;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment27:
                offColorTv();
                fragment27.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_TWENTYSEVEN;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment28:
                offColorTv();
                fragment28.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_TWENTYEIGHT;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment29:
                offColorTv();
                fragment29.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_TWENTYNINE;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment30:
                offColorTv();
                fragment30.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_THIRTY;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment31:
                offColorTv();
                fragment31.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_THIRTYONE;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment32:
                offColorTv();
                fragment32.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_THIRTYTWO;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment33:
                offColorTv();
                fragment33.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_THIRTYTHREE;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment34:
                offColorTv();
                fragment34.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_THIRTYFOUR;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.btn_closed:
                Intent marketLaunch1 = new Intent(Intent.ACTION_VIEW);
                Log.d(TAG, "  " + nextAppUrl);
                marketLaunch1.setData(Uri.parse(nextAppUrl));
                startActivity(marketLaunch1);
                break;

            /*case R.id.btn_mxplayer:
                Intent marketLaunch2 = new Intent(Intent.ACTION_VIEW);
                marketLaunch2.setData(Uri.parse(mxPlayerUrl));
                startActivity(marketLaunch2);
                break;*/
        }
    }

    private Fragment getFragment(int idx) {
        Fragment newFragment = null;
        Bundle args = new Bundle();
        args.putInt("adsCnt", adsCnt);
        args.putString("firstUrl", firstUrl);

        switch (idx) {
            case FRAGMENT_SEARCH:
                newFragment = new FragmentSearch();
                args.putString("baseUrl", fragment99Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_ONE:
                newFragment = new Fragment01();
                args.putString("baseUrl", fragment01Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_TWO:
                newFragment = new Fragment02();
                args.putString("baseUrl", fragment02Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_THREE:
                newFragment = new Fragment02();
                args.putString("baseUrl", fragment03Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_FOUR:
                newFragment = new Fragment02();
                args.putString("baseUrl", fragment04Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_FIVE:
                newFragment = new Fragment02();
                args.putString("baseUrl", fragment05Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_SIX:
                newFragment = new Fragment02();
                args.putString("baseUrl", fragment06Url);
                args.putString("baseClass", ".live06");
                args.putString("baseMenu", "1");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_SEVEN:
                newFragment = new Fragment02();
                args.putString("baseUrl", fragment06Url);
                args.putString("baseClass", ".live07");
                args.putString("baseMenu", "2");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_TWENTYTFOUR:
                newFragment = new Fragment02();
                args.putString("baseUrl", fragment06Url);
                args.putString("baseClass", ".live24");
                args.putString("baseMenu", "3");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_EIGHT:
                newFragment = new Fragment02();
                args.putString("baseUrl", fragment06Url);
                args.putString("baseClass", ".live08");
                args.putString("baseMenu", "4");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_NINE:
                newFragment = new Fragment02();
                args.putString("baseUrl", fragment06Url);
                args.putString("baseClass", ".live09");
                args.putString("baseMenu", "5");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_TEN:
                newFragment = new Fragment02();
                args.putString("baseUrl", fragment06Url);
                args.putString("baseClass", ".live10");
                args.putString("baseMenu", "6");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_ELEVEN:
                newFragment = new Fragment02();
                args.putString("baseUrl", fragment11Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_TWELVE:
                newFragment = new Fragment02();
                args.putString("baseUrl", fragment12Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_THIRTEEN:
                newFragment = new Fragment02();
                args.putString("baseUrl", fragment13Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_FOURTEEN:
                newFragment = new Fragment02();
                args.putString("baseUrl", fragment14Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_FIFTEEN:
                newFragment = new Fragment02();
                args.putString("baseUrl", fragment15Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_SIXTEEN:
                newFragment = new Fragment02();
                args.putString("baseUrl", fragment16Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_SEVENTEEN:
                newFragment = new Fragment02();
                args.putString("baseUrl", fragment17Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_EIGHTTEEN:
                newFragment = new Fragment02();
                args.putString("baseUrl", fragment18Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_NINETEEN:
                newFragment = new Fragment02();
                args.putString("baseUrl", fragment19Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_TWENTY:
                newFragment = new Fragment02();
                args.putString("baseUrl", fragment20Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_TWENTYONE:
                newFragment = new Fragment02();
                args.putString("baseUrl", fragment21Url);
                newFragment.setArguments(args);
                break;

            case FRAGMENT_TWENTYTWO:
                newFragment = new Fragment02();
                args.putString("baseUrl", fragment22Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_TWENTYTHREE:
                newFragment = new Fragment02();
                args.putString("baseUrl", fragment23Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_TWENTYFIVE:
                newFragment = new Fragment02();
                args.putString("baseUrl", fragment25Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_TWENTYSIX:
                newFragment = new Fragment02();
                args.putString("baseUrl", fragment26Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_TWENTYSEVEN:
                newFragment = new Fragment02();
                args.putString("baseUrl", fragment27Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_TWENTYEIGHT:
                newFragment = new Fragment02();
                args.putString("baseUrl", fragment28Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_TWENTYNINE:
                newFragment = new Fragment02();
                args.putString("baseUrl", fragment29Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_THIRTY:
                newFragment = new Fragment02();
                args.putString("baseUrl", fragment30Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_THIRTYONE:
                newFragment = new Fragment02();
                args.putString("baseUrl", fragment31Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_THIRTYTWO:
                newFragment = new Fragment02();
                args.putString("baseUrl", fragment32Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_THIRTYTHREE:
                newFragment = new Fragment02();
                args.putString("baseUrl", fragment33Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_THIRTYFOUR:
                newFragment = new Fragment02();
                args.putString("baseUrl", fragment34Url);
                newFragment.setArguments(args);
                break;
            default:
                Log.d(TAG, "Unhandle case");
                newFragment = new Fragment01();
                args.putString("baseUrl", fragment01Url);
                newFragment.setArguments(args);
                break;
        }

        return newFragment;
    }

    public void fragmentReplace(int reqNewFragmentIndex) {
        Fragment newFragment = null;
        Log.d(TAG, "fragmentReplace " + reqNewFragmentIndex);
        newFragment = getFragment(reqNewFragmentIndex);
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction(); // replace fragment
        transaction.replace(R.id.ll_fragment, newFragment);
        transaction.commit();   // Commit the transaction
    }

    public void offColorTv(){
        fragment99.setBackgroundResource(R.drawable.fragment_borther);
        fragment01.setBackgroundResource(R.drawable.fragment_borther);
        fragment02.setBackgroundResource(R.drawable.fragment_borther);
        fragment03.setBackgroundResource(R.drawable.fragment_borther);
        fragment04.setBackgroundResource(R.drawable.fragment_borther);
        fragment05.setBackgroundResource(R.drawable.fragment_borther);
        fragment06.setBackgroundResource(R.drawable.fragment_borther);
        fragment07.setBackgroundResource(R.drawable.fragment_borther);
        fragment08.setBackgroundResource(R.drawable.fragment_borther);
        fragment09.setBackgroundResource(R.drawable.fragment_borther);
        fragment10.setBackgroundResource(R.drawable.fragment_borther);
        fragment11.setBackgroundResource(R.drawable.fragment_borther);
        fragment12.setBackgroundResource(R.drawable.fragment_borther);
        fragment13.setBackgroundResource(R.drawable.fragment_borther);
        fragment14.setBackgroundResource(R.drawable.fragment_borther);
        fragment15.setBackgroundResource(R.drawable.fragment_borther);
        fragment16.setBackgroundResource(R.drawable.fragment_borther);
        fragment17.setBackgroundResource(R.drawable.fragment_borther);
        fragment18.setBackgroundResource(R.drawable.fragment_borther);
        fragment19.setBackgroundResource(R.drawable.fragment_borther);
        fragment20.setBackgroundResource(R.drawable.fragment_borther);
        fragment21.setBackgroundResource(R.drawable.fragment_borther);
        fragment22.setBackgroundResource(R.drawable.fragment_borther);
        fragment23.setBackgroundResource(R.drawable.fragment_borther);
        fragment24.setBackgroundResource(R.drawable.fragment_borther);
        fragment25.setBackgroundResource(R.drawable.fragment_borther);
        fragment26.setBackgroundResource(R.drawable.fragment_borther);
        fragment27.setBackgroundResource(R.drawable.fragment_borther);
        fragment28.setBackgroundResource(R.drawable.fragment_borther);
        fragment29.setBackgroundResource(R.drawable.fragment_borther);
        fragment30.setBackgroundResource(R.drawable.fragment_borther);
        fragment31.setBackgroundResource(R.drawable.fragment_borther);
        fragment32.setBackgroundResource(R.drawable.fragment_borther);
        fragment33.setBackgroundResource(R.drawable.fragment_borther);
        fragment34.setBackgroundResource(R.drawable.fragment_borther);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.d(TAG, "main activity onResume");

        if(appStatus.equals("1") && d1 != null){
            d2 = new Date();
            long diff = d2.getTime() - d1.getTime();
            long min = diff / 1000 / 60 ;
            Log.d(TAG, "min : " + min);
            if(min >= 30){
                d1 = new Date();
                AdRequest adRequest3 = new AdRequest.Builder().build();
                interstitialAd3 = new InterstitialAd(this);
                interstitialAd3.setAdUnitId("ca-app-pub-9440374750128282/3488411969");
                interstitialAd3.loadAd(adRequest3);
                interstitialAd3.setAdListener(new AdListener(){
                    @Override public void onAdLoaded() {
                        if (interstitialAd3.isLoaded()) {
                            interstitialAd3.show();
                        }
                    }
                });
            }
        }
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this).setTitle("안녕히 가세요.")
                .setMessage("종료 하시겠습니까?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // 확인시 처리 로직
                        if(appStatus.equals("1")){
                            interstitialAd.show();
                        }
                        finish();
                    }})
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // 취소시 처리 로직
                        return;
                    }})
                .show();
    }
}
